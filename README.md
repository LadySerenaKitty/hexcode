# hexcode

Simple utilities for hex-encoding data.  It is **not** crypto.  Four tools are included:
+ **enhex** - Converts input into hex (for online posting)
+ **dehex** - Converts hex data back to original form


## Installation
[![Packaging status](https://repology.org/badge/vertical-allrepos/hexcode.svg)](https://repology.org/project/hexcode/versions)

### FreeBSD
Ports:
```
cd /usr/ports/textproc/hexcode
make install clean
```
Packages:
```
pkg install hexcode
```

## Usage
Usage is simple and straightforward.  Without any arguments, it will read from `stdin`.  Only 1 argument is supported
and that would be an input file.  All output goes to `stdout`, so if you want the output to go to a file, use shell
redirection.


## Examples
```
echo "Mew mew" | enhex
```
Output:
```
4d 65 77 20 6d 65 77 0a
```
---
```
echo "4d 65 77 20 6d 65 77 0a" | dehex
```
Output:
```
Mew mew
```
---
```
enhex some-file > output.txt
```

