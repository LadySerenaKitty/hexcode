#include "hexcode.h"
#include <fstream>

int main(int argc, char** argv) {

	if (argc <= 1) { hexcode_enhex(std::cin, std::cout); }
	else {
		std::ifstream is(argv[1], std::ios::binary | std::ios::in);
		hexcode_enhex(is, std::cout);
		is.close();
	}

	return 0;
}

