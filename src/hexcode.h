#include <iostream>

#ifdef __cplusplus
extern "C" {
#endif

void hexcode_enhex(std::istream &is, std::ostream &os);
void hexcode_dehex(std::istream &is, std::ostream &os);

#ifdef __cplusplus
}
#endif

