#include "hexcode.h"
#include <iomanip>
#include <string>

#ifdef __cplusplus
extern "C" {
#endif

void hexcode_enhex(std::istream &is, std::ostream &os) {
	os << std::setfill('0');
	unsigned char chr = 0;
	int a = 0;
	while(is.good()) {
		chr = is.get();
		if (is.fail()) { return; }
		os << std::setw(2) << std::hex << (int)chr;
		a++;
		if ((a % 16) == 0) {
			os << "\n";
		}
		else { os << " "; }
		if ((a % 512) == 0) { os << "\n"; }
	}
}

void hexcode_dehex(std::istream &is, std::ostream &os) {
	unsigned char chr = 0;
	char in[3] = {0,0,0};
	bool first = false;
	bool second = false;
	while (!(is >> chr).eof()) {
		if (is.fail()) { return; }
		if (('0' <= chr && chr <= '9') || ('a' <= chr && chr <= 'f') || ('A' <= chr && chr <= 'F')) {
			if (!first) { in[0] = chr; first = true; }
			else if (!second) { in[1] = chr; second = true; }
		}
		if (first && second) {
			std::string str(in);
			os << (unsigned char)std::stoi(str, nullptr, 16);
			first = false;
			second = false;
		}
	}
}

#ifdef __cplusplus
}
#endif

